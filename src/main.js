import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.filter("counted", value => value + " (" + value.length.toString() + ")");
new Vue({
  render: h => h(App)
}).$mount("#app");
